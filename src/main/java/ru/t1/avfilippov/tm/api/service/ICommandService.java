package ru.t1.avfilippov.tm.api.service;

import ru.t1.avfilippov.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
